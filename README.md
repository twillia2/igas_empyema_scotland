# iGAS_empyema_Scotland

Supporting data for study examining iGAS empyema cases in children and young people in Scotland.

Population tables used are provided by National Records of Scotland at the following link: 
https://www.nrscotland.gov.uk/statistics-and-data/statistics/statistics-by-theme/population/population-estimates
