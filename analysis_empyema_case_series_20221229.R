#t test for pleural culture positive vs negative cases
setwd(""")

#input data
culture_pos_vs_time  <- read.table("pleural_culture_data.txt", header = T,
                                       sep = "\t")

#run unpaired two-sided students t-test
culture_positive <- culture_pos_vs_time$Pleural.culture.positive
culture_negative <- culture_pos_vs_time$Pleural.culture.negative

t.test(culture_positive, culture_negative,
       alternative = c("two.sided"),
       mu = 0, paired = FALSE, var.equal = FALSE,
       conf.level = 0.95)

#Calculate 95% confidence interval for previous years
#import table with data
incidence_rate_data <- read.table("empyema_incidence_rates.txt",header = T,
                                  sep = "\t")
#calculate incidence per million
incidence_rate_data$rate_per_million <- 1E6*(incidence_rate_data$Number_of_empyema_cases/incidence_rate_data$Population_at_risk_actual)

#calculate mean 2017-2019, 2021
years_to_calculate_rate_from <- c(incidence_rate_data$rate_per_million[c(1,2,3,5)])

#calculate mean
mean_empyema_rate <- mean(years_to_calculate_rate_from)

#calculate n
number_years <- length(years_to_calculate_rate_from)

#find the standard deviation
standard_deviation <- sd(years_to_calculate_rate_from)

#find the standard error
standard_error <- standard_deviation / sqrt(number_years)

#calculate t score
alpha <- 0.05
degrees_of_freedom <- number_years - 1
t_score <- qt(p=alpha/2, df=degrees_of_freedom,lower.tail=F)

#calculate margin of error
margin_error <- t_score * standard_error

#calculate the lower bound 
lower_bound <- mean_empyema_rate - margin_error

#calculate the upper bound
upper_bound <- mean_empyema_rate + margin_error

#print outputs
print(c(mean_empyema_rate,lower_bound,upper_bound))

